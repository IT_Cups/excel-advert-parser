﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Windows;

namespace Excel
{
	/// <summary>
	/// Data class that holds all ad entries in easily accessible form.
	/// </summary>
	public class AdvertData
	{
		public struct AdDataEntry
		{
			public DateTime date;
			public string country;
			public int impressions;
			public int clicks;
			public double clickCost;
			public int convertions;
			public double convertRate;
			public double costPerConvert;

			public override string ToString() =>
				$"date: {date}; country: {country}; impressions: {impressions}; clicks: {clicks}; clickCost: {clickCost}; convertions: {convertions}; convertRate: {convertRate}; costPerConvert: {costPerConvert}";
		}

		/// <summary>
		/// Enum used for navigation in dictionary when calculating monthly profit
		/// </summary>
		private enum IncomeStatement
		{
			Positive,
			Negative
		}

		/// <summary>
		/// List with all advert entries from excel file
		/// </summary>
		public List<AdDataEntry> Items { get;  private set; } =  new List<AdDataEntry>();

		/// <summary>
		/// Adds advert entry in the list
		/// </summary>
		public void AddEntry(DateTime date, string country, int impressions, int clicks, double clickCost, int convertions, double convertRate, double costPerConvert)
		{
			AdDataEntry e = new AdDataEntry();
			e.date = date.Date;
			e.country = country;
			e.impressions = impressions;
			e.clicks = clicks;
			e.clickCost = clickCost;
			e.convertions = convertions;
			e.convertRate = convertRate;
			e.costPerConvert = costPerConvert;
			Items.Add(e);
		}
		/// <summary>
		/// Add all entries from DataTable to list using start position in the table and keys for column navigation 
		/// </summary>
		/// <param name="start">Starting position (X,Y) begining from top left corner<</param>
		/// <param name="table">Data variable containing a table</param>
		/// <param name="keys">Keys with column indexes that used to navigate the table </param>
		public void AddData(DataTable table, Vector start, Dictionary<string, int> keys)
		{
			if (Items.Count > 0)
				ClearItems();
			for (int i = (int)start.Y + 1; i < table.Rows.Count; i++)
			{
				DataRow row = table.Rows[i];
				if (row.ItemArray[keys["Country/Territory"]].ToString() == "")
					return;
				AddEntry(
				Convert.ToDateTime(row.ItemArray[keys["Date"]], CultureInfo.InvariantCulture),
				Convert.ToString(row.ItemArray[keys["Country/Territory"]]),
				Convert.ToInt32(row.ItemArray[keys["Impressions"]]),
				Convert.ToInt32(row.ItemArray[keys["Clicks"]]),
				Convert.ToDouble(row.ItemArray[keys["Cost"]]),
				Convert.ToInt32(row.ItemArray[keys["Conversions"]]),
				Convert.ToDouble(row.ItemArray[keys["Conv. Rate"]]),
				Convert.ToDouble(row.ItemArray[keys["Cost/Conv."]]));
			}
		}

		/// <summary>
		/// Summarise revenue and losses.
		/// </summary>
		/// <param name="itemCost">Cost of one item in dollars</param>
		/// <returns>Dictionary containing revenue and losses with country names as keys</returns>
		private Dictionary<string, Dictionary<IncomeStatement, double>> GetIncomeStatement(double itemCost)
		{

			Dictionary<string, Dictionary<IncomeStatement, double>> IncomeData = new Dictionary<string, Dictionary<IncomeStatement, double>>();
			foreach (var entry in Items)
			{
				if (!IncomeData.ContainsKey(entry.country))
				{
					var newValue = new Dictionary<IncomeStatement, double>();
					newValue[IncomeStatement.Positive] = 0;
					newValue[IncomeStatement.Negative] = 0;
					IncomeData[entry.country] = newValue;

				}

				// доход на страну
				double countryRevenue = entry.convertions * itemCost;
				// Траты на рекламу
				double countryExpense = entry.clickCost;
				IncomeData[entry.country][IncomeStatement.Positive] += countryRevenue;
				IncomeData[entry.country][IncomeStatement.Negative] += countryExpense;


			}
			return IncomeData;
		}

		/// <summary>
		/// Main solution for the task
		/// Calculates profit considering taxes and compares it to losses for each country in stored data
		/// </summary>
		/// <param name="itemCost">Cost of one item in dollars<</param>
		/// <param name="tax">Taxes in percents (0-100) </param>
		/// <returns>List of countries where profit is less than losses</returns>
		public List<string> GetUnprofitableContries(double itemCost, double tax)
		{
			List<string> countries = new List<string>();
			var incomeData = GetIncomeStatement(itemCost);
			foreach (var country in incomeData.Keys)
			{
				var d = incomeData[country];
				/*
					Проанализировать отчет, содержащий результаты онлайновой рекламы, для каждой страны и сделать вывод, реклама в каких странах является убыточной.  
					Нужно принять во внимание, что постоянные расходы компании NNN составляют 30% от ежемесячного дохода. 
				*/
				if (d[IncomeStatement.Positive] - d[IncomeStatement.Positive]* (tax/100) < d[IncomeStatement.Negative])
					countries.Add(country);
			}
			return countries;
		}

		/// <summary>
		/// Calculates profit considering taxes and compares it to losses for each country in stored data
		/// </summary>
		/// <param name="itemCost">Cost of one item in dollars<</param>
		/// <param name="tax">Taxes in percents (0-100) </param>
		/// <returns>List of countries where profit is more or equal than losses</returns>
		public List<string> GetProfitableContries(double itemCost, double tax)
		{
			List<string> countries = new List<string>();
			var incomeData = GetIncomeStatement(itemCost);
			foreach (var country in incomeData.Keys)
			{
				var d = incomeData[country];
				if (d[IncomeStatement.Positive] - d[IncomeStatement.Positive] * (tax / 100) >= d[IncomeStatement.Negative])
					countries.Add(country);
			}
			return countries;
		}

		public void ClearItems()
		{
			Items = new List<AdDataEntry>();
		}

	}
}
