﻿using ExcelDataReader;
using System;
using System.Data;
using System.IO;
using System.Windows;

namespace Excel
{
	public static class ExcelHandler
	{
		/// <summary>
		/// This method will attempt excel file and return DataSet with file content 
		/// </summary>
		/// <param name="filePath">Path to the file in OS</param>
		/// <returns></returns>
		static public DataSet GetDataSet(String filePath)
		{
			try
			{
				using (var stream = File.Open(filePath, FileMode.Open, FileAccess.Read))
				{
					// Auto-detect format, supports:
					//  - Binary Excel files (2.0-2003 format; *.xls)
					//  - OpenXml Excel files (2007 format; *.xlsx, *.xlsb)
					using (var reader = ExcelReaderFactory.CreateReader(stream))
					{
						// Choose one of either 1 or 2:

						// 1. Use the reader methods
						do
						{
							while (reader.Read())
							{
								// reader.GetDouble(0);
							}
						} while (reader.NextResult());

						// 2. Use the AsDataSet extension method
						var result = reader.AsDataSet();
						return result;
						// The result of each spreadsheet is in result.Tables
					}
				}
			}
			catch (IOException)
			{
				MessageBox.Show("Файл не может быть открыт, ошибка чтения (возможно файл открыт в другой программе)",
										  "Ошибка",
										  MessageBoxButton.OK,
										  MessageBoxImage.Error);
				
			}
			return null;
		}

		
	}
}
