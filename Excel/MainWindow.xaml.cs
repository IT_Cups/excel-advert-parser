﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Text.RegularExpressions;


namespace Excel
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	/// 

	

	public partial class MainWindow : Window
	{
		/// <summary>
		/// Main data class that holds all ad entries in easily accessible form.
		/// </summary>
		public AdvertData AdData { get; private set; } = new AdvertData();
		/// <summary>
		/// Keywords that used to navigate DataTable as well as their column position (-1 means column wasn't found)
		/// </summary>
		Dictionary<string, int> keysToFind = new Dictionary<string, int>() { { "Date", -1 }, { "Country/Territory", -1 }, { "Impressions", -1 }, { "Clicks", -1 }, { "Cost", -1 }, { "Conversions", -1 }, { "Conv. Rate", -1 }, { "Cost/Conv.", -1 } };
		/// <summary>
		/// Starting position when actual data begins in the table
		/// </summary>
		Vector? startPos = null;

		public MainWindow()
		{
			InitializeComponent();
		}
		/// <summary>
		/// Opens file and pupulates property AdData from
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OpenFile_Click(object sender, RoutedEventArgs e)
		{
			OpenFileDialog openFileDialog = new OpenFileDialog
			{
				InitialDirectory = "c:\\",
				Filter = "xls file (*.xls)|*.xls",
				FilterIndex = 2,
				RestoreDirectory = true
			};


			if (openFileDialog.ShowDialog() == true )
			{
				//Get the path of specified file
				var filePath = openFileDialog.FileName;
				// Using ExcelDataReader to open file
				DataSet data = ExcelHandler.GetDataSet(filePath);
				// file read error
				if (data == null)
				{
					if(AdData.Items.Count > 0)
						SetInterfaceLock(false);
					else
						SetInterfaceLock(true);
					return;
				}
					
				
				DataView dataView = data.Tables[0].AsDataView();
				// Binding data to DataGrid
				MainGrid.ItemsSource = dataView;
				// Finding start position of raw data using predefined keys
				startPos = FindStartPos(dataView.Table, keysToFind.Keys.ToList<string>());
				if (!startPos.HasValue)
				{
					MessageBox.Show("Не найдено начало таблицы по ключам :" + string.Join(", ", keysToFind.Keys),
										  "Ошибка",
										  MessageBoxButton.OK,
										  MessageBoxImage.Error);
					SetInterfaceLock(true);
					return;
				}
				// Finding column position for keys
				if (!FindKeysColumns(dataView.Table, startPos.Value, ref keysToFind))
					return;
				// Populating our AdvertData class
				AdData.AddData(dataView.Table, startPos.Value, keysToFind);


				SetInterfaceLock(false);
			}
			else
			{
				if (AdData.Items.Count > 0)
					SetInterfaceLock(false);
				else
					SetInterfaceLock(true);
			}
		}

		/// <summary>
		/// Disable some interface controls when passed true
		/// </summary>
		/// <param name="value"></param>
		private void SetInterfaceLock(bool value)
		{
			CalcButton.IsEnabled = !value;
			PercentBox.IsEnabled = !value;
			ItemCostBox.IsEnabled = !value;
		}

		/// <summary>
		/// Finding starting position of actual data in table
		/// </summary>
		/// <param name="table">Data variable containing a table</param>
		/// <param name="keys">Keys that are used to find starting position</param>
		/// <returns>Nullable Vector variable with starting position (X,Y) begining in top left corner</returns>
		private Vector? FindStartPos(DataTable table, List<string> keys)
		{
			for (int i = 0; i < table.Rows.Count; i++)
			{
				DataRow row = table.Rows[i];
				//Console.WriteLine( i+ ":" + row.ItemArray.Length);
				for (int j = 0; j < row.ItemArray.Length; j++)
				{
					foreach (var key in keys)
						if (row.ItemArray[j].ToString().Contains(key))
						{
							var pos = new Vector(j, i);
							return pos;
						}
				}
			}
			return null;
		}

		/// <summary>
		/// Finds column indexes for each key used which used to navigate table
		/// </summary>
		/// <param name="table">Data variable containing a table</param>
		/// <param name="pos">Starting position (X,Y) begining from top left corner</param>
		/// <param name="keys">Reference to keys dictionary to set column indexes inside</param>
		/// <returns>Returns false if atleast 1 key was not found</returns>
		private bool FindKeysColumns(DataTable table, Vector pos, ref Dictionary<string, int> keys)
		{
			DataRow row = table.Rows[(int)pos.Y];

			//Console.WriteLine( i+ ":" + row.ItemArray.Length);

			Console.WriteLine("pos" + pos.X + ":"+ pos.Y);
			
			List<string> keyCopy = keys.Keys.ToList<string>();
			
			for (int j = (int)pos.X; j < row.ItemArray.Length; j++)
				{
				Console.WriteLine("data " + string.Join(", ", row.ItemArray));
				foreach (var key in keyCopy)
					if (row.ItemArray[j].ToString().Contains(key))
					{
						keys[key] = j;
						keyCopy.Remove(key);
						break;
					}
			}
			if(keyCopy.Count > 0)
				{
				MessageBox.Show("В таблице не были найдены следущие ключи : " + string.Join(", ",keyCopy),
									  "Ошибка",
									  MessageBoxButton.OK,
									  MessageBoxImage.Error);
				return false;
			}
			return true;
		}


		/// <summary>
		/// Selects rows in mainGrid using selected value in listProfit and moves focus
		/// </summary>
		private void CalcButton_Click(object sender, RoutedEventArgs e)
		{
			if (double.TryParse(PercentBox.Text, out double percent) && double.TryParse(ItemCostBox.Text, out double itemCost))
			{
				var cLoss = AdData.GetUnprofitableContries(itemCost, percent);
				ListLoss.ItemsSource = cLoss;
				var cProfit = AdData.GetProfitableContries(itemCost, percent);
				ListProfit.ItemsSource = cProfit;
			}

		}

		/// <summary>
		/// Selects rows in mainGrid using selected value in listLoss and moves focus
		/// </summary>
		private void ListLossSelection_Changed(object sender, SelectionChangedEventArgs e)
		{
			if (ListLoss.SelectedItem == null)
				return;
			var dataTable = ((DataView)MainGrid.ItemsSource).Table;
			MainGrid.SelectedItem = null;
			MainGrid.SelectedItems.Clear();
			for (int i = (int)startPos.Value.Y + 1; i < dataTable.Rows.Count; i++)
			{
				DataRow row = dataTable.Rows[i];
				if (row.ItemArray[keysToFind["Country/Territory"]].ToString() == ListLoss.SelectedItem.ToString())
				{
					MainGrid.SelectedItems.Add(MainGrid.Items[i]);
				}
			}
			MainGrid.Focus();

		}
		/// <summary>
		/// Selects rows in mainGrid with selected value in listProfit and moves focus
		/// </summary>
		private void ListProfitSelection_Changed(object sender, SelectionChangedEventArgs e)
		{
			if (ListProfit.SelectedItem == null)
				return;
			var dataTable = ((DataView)MainGrid.ItemsSource).Table;
			MainGrid.SelectedItem = null;
			MainGrid.SelectedItems.Clear();
			for (int i = (int)startPos.Value.Y + 1; i < dataTable.Rows.Count; i++)
			{
				DataRow row = dataTable.Rows[i];
				if (row.ItemArray[keysToFind["Country/Territory"]].ToString() == ListProfit.SelectedItem.ToString())
				{
					MainGrid.SelectedItems.Add(MainGrid.Items[i]);
				}
			}
			MainGrid.Focus();

		}
		/// <summary>
		/// Validates text after input
		/// </summary>
		private void PercentBox_LostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
		{
			if (Double.TryParse(PercentBox.Text, out double value))
			{
				PercentBox.Text = ((value < 0) ? 0 : (value > 100) ? 100 : value).ToString();
				e.Handled = true;
				return;
			}

			e.Handled = false;
			
		}

		/// <summary>
		/// Validates text after input
		/// </summary>
		private void ItemCostBox_LostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
		{
			if (Double.TryParse(ItemCostBox.Text, out double value))
			{
				ItemCostBox.Text = ((value < 0) ? 0 : value).ToString();
				e.Handled = true;
				return;
			}

			e.Handled = false;
		}

		/// <summary>
		/// Opens info window
		/// </summary>
		private void InfoButton_Click(object sender, RoutedEventArgs e)
		{
			Window1 win1 = new Window1();
			win1.Show();
		}



		/// <summary>
		/// Validates text input for textbox
		/// </summary>
		private void NumberValidation(object sender, TextCompositionEventArgs e)
		{
			var regex = new Regex("^[.]|[0-9]");
			if (!regex.IsMatch(e.Text))
			{
				e.Handled = true;
				return;

			}

			e.Handled = false;
		}
	}
}
